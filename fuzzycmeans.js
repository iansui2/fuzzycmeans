var underscore = require('underscore');
var indexOf = require('underscore').indexOf;
var mj = require('mathjs');
var nj = require('numjs');
var list = require('collections/list.js');

const dfd = require('danfojs-node');
var lodash = require("lodash");
const { sum, add } = require("lodash");

const clusterNumber = 4;
const users = 1;

// Gets the data and transforms it to a data frame
const getData = () => {
    
    var jobs = [
        "IT Manager",
        "System Administrator",
        "Software Developer",
        "Android Developer"
    ];

    var highestPercentage = [
        1
    ];

    var averageRatings = [
        8
    ];

    var data = [];

    for (var i = 0; i < users; i++) {
        data.push([highestPercentage[i], averageRatings[i]]);
    }

    /*
       DataFrame is a 2-dimensional labeled data structure 
       with columns of potentially different types. 
       You can think of it like a spreadsheet or SQL table.

       Transform json data into a dataframe
    */
    var df = new dfd.DataFrame(data);
    return data;
};

// /*
//     Performs the Fuzzy C-Means Algorithm based on the following:
//     dataframe: the dataset
//     clusters: the number of clusters
//     m: the fuzzifier, the recommended number is 2
// */
const fuzzyCMeans = (dataframe, clusters=clusterNumber, m=2) => {
    var clusterLabels = [];
    
    var datapoints = dataframe.length;
    var features = 2;

    var maxIterations = datapoints;
    var currentIteration = 0;

    var meanOfSkills = [0.8, 0, 0, 0];
    var meanOfRatings = [7.5, 0, 0, 0];

    var clusterCenters = [];

    for (var i = 0; i < clusters; i++) {
        clusterCenters.push([meanOfSkills[i], meanOfRatings[i]]);
    }

    // Step 1: Initialize Membership Values
    var membershipValues = initializeMembershipValues(clusters);

    while (currentIteration < maxIterations) {
        // Step 2: Calculate Cluster Centers or Centroids
        // cluster_centers = calculateClusterCenters(clusters, membershipValues, dataframe, datapoints, clusterCenters, m);
        // Step 3: Update Membership Values
        // membershipValues = updateMembershipValues(clusters, membershipValues, dataframe, datapoints, features, clusterCenters, m);
        // Get cluster labels
        clusterLabels = getClusterLabels(datapoints, membershipValues);
        currentIteration += 1  
    }

    return membershipValues;
};

// Step 1: Initialize Membership Values
const initializeMembershipValues = (clusters) => {
   var membershipValues = [];

   var membershipValuesSum = 0;

   while (membershipValuesSum !== 1) {
       membershipValues = [];
       membershipValues.push([...Array(clusters)].map(() => Math.random()));
       var membershipValuesFlat = membershipValues.flat()
       membershipValuesSum = membershipValuesFlat.reduce(function (a, b) {
            return a + b;
       }, 0);
   }

    return membershipValues
}

// Step 2: Calculate Cluster Centers or Centroids
const calculateClusterCenters = (clusters, membershipValues, dataframe, datapoints, clusterCenters, m) => {
    for (j = 0; j < clusters.length; j++) {
        var denominator = sum(Math.pow(membershipValues[j][j], m));
        var numerator = 0;
        for (i = 0; i < datapoints.length; i++) {
            var result = (Math.pow(membershipValues[i][j])) * (dataframe[i]);
            numerator += result;
        }
        var initialClusterCenters = numerator / denominator;
        clusterCenters[j] = initialClusterCenters;
    }
    return clusterCenters;
}

// Step 3: Update Membership Values
const updateMembershipValues = (clusters, membershipValues, dataframe, datapoints, features, clusterCenters, m) => {
    var p = parseFloat(1/(m-1));
    for (i = 0; i < datapoints.length; i++) {
        var denominator = 0;

        for (j = 0; j < clusters.length; j++) {
            denominator += getEuclideanDistance(dataframe[i], clusterCenters[j][features]);
        }

        for (j = 0; j < clusters.length; j++) {
            var updateMembershipValues = 1/Math.pow((getEuclideanDistance(dataframe[i], clusterCenters[j][features]) / denominator), p);
            membershipValues[i][j] = updateMembershipValues;
        }
    }
    return membershipValues;
}

// Get Euclidean Distance
const getEuclideanDistance = (dataframe, clusterCenters) => {
    return dataframe.map((i, j) => Math.abs( i - clusterCenters[j] ) ** 2)
                    .reduce((sum, now) => sum + now )
                    ** 1/2
}

// Get Cluster Labels
const getClusterLabels = (datapoints, membershipValues) => {
    var clusterLabels = [];
    for (i = 0; i < datapoints.length; i++) {
        var highestValue = Math.max(membershipValues[i]);
        clusterLabels.push(highestValue);
    }
    return clusterLabels
}

var dataset = getData();

var clusterLabelsResult = fuzzyCMeans(dataset);

module.exports = {clusterLabelsResult};


