import scipy.spatial
import numpy as np
import fuzzyCMeans

u = fuzzyCMeans.membership_values
x = fuzzyCMeans.dataset
v = fuzzyCMeans.cluster_centers
m = fuzzyCMeans.m

def kwon(u, x, v, m):
    n = x.shape[0]
    c = v.shape[1]

    uij = u ** m

    dxv = get_spatial_distance(x, v, m)
    dvv = get_spatial_distance(v, v, m)

    dvv[dvv == 0.0] = np.inf

    vmean = np.mean(v)
    
    vdiff = np.sum((v - vmean) ** m)

    kwon = (np.sum(uij.T @ dxv) + np.sum(vdiff)/c) / np.min(dvv)                
   
    return kwon

def get_spatial_distance(x, y, m):
    return scipy.spatial.distance.cdist(x, y) ** m

kwon_result = kwon(u, x, v, m)

print(kwon_result)    