import sys
import json
import math
import numpy as np
import pandas as pd
import math

cluster_number = 2

# value = json.loads(sys.argv[1])

# Get the data from the users in JavaScript
def get_data():
    # jobs = json.dumps(value["clusterLabels"])
    # highest_percentage = value["highestPercentage"]
    # average_ratings = value["averageRatings"]
    # educational_percentage = value["educationalPercentage"]
    # work_experience_percentage = value["workExperiencePercentage"]

    # jobs = [
    #     'IT Manager',
    #     'System Administrator',
    #     'Software Developer',
    #     'Android Developer',
    #     'Full Stack Developer',
    #     'Web Developer',
    #     'Back End Web Developer',
    #     'Software Tester',
    #     'IT Specialist',
    #     'IT Intern',
    #     'Senior Software Engineer',
    #     'Network Security Engineer Level 1',   
    #     'Systems Engineer',
    #     'Software Development Analyst',        
    #     'Application IT Engineer',
    #     'Copywriter',
    #     'Digital Marketing Assistant',
    #     'Multimedia Supervisor',
    #     'Communications Assistant Manager',        
    #     'Videographer/Photographer (Multimedia Artist)',
    #     'Digital Media UI/UX Senior Designer',     
    #     'SOCIAL MEDIA SPECIALIST',
    #     'Technical Marketing Specialist',
    #     'Product Designer',
    #     'Writer Producer'
    # ]

    # Ian                  
    # highest_percentage = [ 2.5, 10, 6.7, 2.5, 6, 6, 6, 2, 1.4, 0, 0, 1.4, 4, 5, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ]
    # average_ratings = [ 3, 5, 5.67, 7, 5, 5.33, 7, 6, 7, 0, 0, 3, 6, 5, 5.67, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ]
    # educational_percentage = [ 5, 10, 10, 5, 10, 10, 10, 5, 5, 5.8, 5.8, 5, 5, 10, 10, 5.8, 5.8, 5.8, 5.8, 5.8, 5.8, 5.8, 5.8, 5.8, 5.8 ]
    # work_experience_percentage = [ 3, 5, 5, 3, 5, 5, 10, 3, 3, 3, 2, 3, 3, 5, 5, 3, 3, 3, 2, 3, 2, 3, 3, 3, 3 ]

    # highest_percentage = [ 2.5, 10, 3.3, 2.5, 6, 6, 6, 2, 1.4, 1.4, 4, 2.5, 5 ]
    # average_ratings = [ 3, 5, 5.67, 7, 5, 5.33, 7, 6, 7, 3, 6, 5, 5.67 ]
    # educational_percentage = [ 7, 10, 10, 7, 10, 10, 10, 7, 7, 7, 7, 10, 10 ]
    # work_experience_percentage = [ 2, 5, 3, 3, 5, 5, 10, 3, 3, 3, 3, 3, 5 ]

    # Jhun
    # highest_percentage = [ 7.5, 10, 8.9, 7.5, 6, 4, 6, 0, 1.4, 0, 0, 0, 4, 5, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ]
    # average_ratings = [ 7.67, 8, 8, 8, 8.67, 8.5, 7, 0, 7, 0, 0, 0, 8, 9, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ]
    # educational_percentage = [ 7, 10, 10, 10, 10, 5, 10, 5, 5, 5, 5, 5, 5, 10, 10, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 ]
    # work_experience_percentage = [ 3, 3, 10, 5, 5, 3, 5, 3, 3, 3, 2, 3, 3, 5, 5, 3, 3, 2, 2, 3, 2, 3, 3, 2, 3 ]

    highest_percentage = [ 7.5, 10, 4.4, 7.5, 6, 4, 6, 1.4, 4, 2.5, 5 ]
    average_ratings = [ 7.67, 8, 8, 8, 8.67, 8.5, 7, 7, 8, 9, 8 ]
    educational_percentage = [ 7, 10, 7, 10, 10, 7, 10, 7, 7, 7, 10 ]
    work_experience_percentage = [ 3, 3, 7, 5, 5, 3, 5, 3, 2, 3, 5 ]

    # James
    # highest_percentage = [ 2.5, 10, 2.2, 0, 0, 2, 2, 0, 0, 0, 0, 1.4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ]
    # average_ratings = [ 9, 6, 7, 0, 0, 6, 6, 0, 0, 0, 0, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ]
    # educational_percentage = [ 5, 10, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 ]
    # work_experience_percentage = [ 5, 5, 3, 3, 3, 3, 3, 3, 3, 3, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3 ]

    # highest_percentage = [ 2.5, 10, 1.1, 2, 2, 1.4 ]
    # average_ratings = [ 9, 6, 7, 6, 6, 9 ]
    # educational_percentage = [ 5, 10, 7, 7, 7, 7 ]
    # work_experience_percentage = [ 7, 5, 3, 3, 3, 3 ]

    # Kezia
    # highest_percentage = [ 0, 10, 2.2, 0, 0, 2, 2, 0, 0, 0, 0, 0, 2, 2.5, 1.7, 2.5, 0, 0, 0, 0, 0, 0, 0, 0, 0 ]
    # average_ratings = [ 0, 9, 6, 0, 0, 9, 6, 0, 0, 0, 0, 0, 6, 10, 6, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0 ]
    # educational_percentage = [ 5, 10, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 ]
    # work_experience_percentage = [ 1, 2, 3, 1, 1, 1, 3, 1, 1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ]

    # highest_percentage = [ 10, 1.1, 2, 2, 2, 1.3, 1.7, 2.5 ]
    # average_ratings = [ 9, 6, 9, 6, 6, 10, 6, 8 ]
    # educational_percentage = [ 10, 7, 7, 7, 7, 7, 7, 5 ]
    # work_experience_percentage = [ 2, 3, 1, 3, 1, 1, 1, 1 ]

    # Mark
    # highest_percentage = [ 0, 10, 4.4, 0, 6, 4, 2, 0, 0, 0, 0, 0, 2, 5, 3.3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ]
    # average_ratings = [ 0, 6, 8, 0, 8.33, 7.5, 7, 0, 0, 0, 0, 0, 8, 8.5, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ]
    # educational_percentage = [ 5, 10, 5, 5, 10, 5, 5, 5, 5, 5, 5, 5, 5, 10, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 ]
    # work_experience_percentage = [ 2, 3, 3, 3, 5, 3, 3, 3, 3, 3, 2, 3, 3, 5, 3, 3, 3, 2, 2, 3, 2, 3, 3, 2, 3 ]

    # highest_percentage = [ 10, 2.2, 6, 4, 2, 2, 2.5, 3.3 ]
    # average_ratings = [ 6, 8, 8.33, 7.5, 7, 8, 8.5, 8 ]
    # educational_percentage = [ 10, 7, 10, 7, 7, 7, 10, 7 ]
    # work_experience_percentage = [ 3, 3, 5, 3, 3, 2, 3, 3 ]

    # Paul
    # highest_percentage = [ 5, 0, 2.2, 2.5, 4, 2, 0, 0, 0, 0, 0, 1.4, 0, 2.5, 1.7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ]
    # average_ratings = [ 7.5, 0, 8, 4, 8.5, 9, 0, 0, 0, 0, 0, 7, 0, 9, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ]
    # educational_percentage = [ 6, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 ]
    # work_experience_percentage = [ 3, 2, 5, 3, 3, 3, 3, 3, 3, 3, 2, 3, 2, 3, 3, 3, 3, 2, 2, 3, 2, 3, 3, 2, 3 ]

    # highest_percentage = [ 5, 1.1, 2.5, 4, 2, 1.4, 1.3, 1.7 ]
    # average_ratings = [ 7.5, 8, 4, 8.5, 9, 7, 9, 8 ]
    # educational_percentage = [ 7, 7, 7, 7, 7, 7, 7, 7]
    # work_experience_percentage = [ 3, 7, 3, 3, 3, 3, 3, 3 ]

    data = np.column_stack((highest_percentage, average_ratings, educational_percentage, work_experience_percentage))

    df = pd.DataFrame(data=data)

    data_points = len(df)
    return data, data_points

# Implement Fuzzy C-Means Algorithm
def get_fuzzy_c_means_result(dataset, data_points, c=cluster_number, m=2):
    # mean_of_skills = value["meanOfSkills"]
    # mean_of_ratings = value["meanOfRatings"]
    # mean_of_educational_percentage = value["meanOfEducationalPercentage"]
    # mean_of_work_percentage = value["meanOfWorkPercentage"]

    # less_of_skills = value["lessOfSkills"]
    # less_of_ratings = value["lessOfRatings"]
    # less_of_educational_percentage = value["lessOfEducationalPercentage"]
    # less_of_work_percentage = value["lessOfWorkPercentage"]

    # mean_of_skills = [ 5.4 ]
    # mean_of_ratings = [ 5 ]
    # mean_of_educational_percentage = [ 5.03 ]
    # mean_of_work_percentage = [ 4.24 ]

    # less_of_skills = [ 2.7 ]
    # less_of_ratings = [ 2.5 ]
    # less_of_educational_percentage = [ 2.52 ]
    # less_of_work_percentage = [ 2.12 ]

    # Ian
    # mean_of_skills = [ 5.46 ]
    # mean_of_ratings = [ 4.31 ]
    # mean_of_educational_percentage = [ 5.06 ]
    # mean_of_work_percentage = [ 4.31 ]

    # less_of_skills = [ 2.73 ]
    # less_of_ratings = [ 2.15 ]
    # less_of_educational_percentage = [ 2.53 ]
    # less_of_work_percentage = [ 2.15 ]

    # Jhun
    mean_of_skills = [ 5.36 ]
    mean_of_ratings = [ 4.36 ]
    mean_of_educational_percentage = [ 5.07 ]
    mean_of_work_percentage = [ 4.36 ]

    less_of_skills = [ 2.68 ]
    less_of_ratings = [ 2.18 ]
    less_of_educational_percentage = [ 2.54 ]
    less_of_work_percentage = [ 2.18 ]

    # James
    # mean_of_skills = [ 5.17 ]
    # mean_of_ratings = [ 4.17 ]
    # mean_of_educational_percentage = [ 5.13 ]
    # mean_of_work_percentage = [ 4.17 ]

    # less_of_skills = [ 2.59 ]
    # less_of_ratings = [ 2.09 ]
    # less_of_educational_percentage = [ 2.57 ]
    # less_of_work_percentage = [ 2.09 ]

    # Kezia 
    # mean_of_skills = [ 5.38 ]
    # mean_of_ratings = [ 4.13 ]
    # mean_of_educational_percentage = [ 5 ]
    # mean_of_work_percentage = [ 4.13 ]

    # less_of_skills = [ 2.69 ]
    # less_of_ratings = [ 2.07 ]
    # less_of_educational_percentage = [ 2.5 ]
    # less_of_work_percentage = [ 2.07 ]

    # Mark
    # mean_of_skills = [ 5.5 ]
    # mean_of_ratings = [ 4.38 ]
    # mean_of_educational_percentage = [ 5 ]
    # mean_of_work_percentage = [ 4.38 ]

    # less_of_skills = [ 2.75 ]
    # less_of_ratings = [ 2.19 ]
    # less_of_educational_percentage = [ 2.5 ]
    # less_of_work_percentage = [ 2.19 ]

    # Paul
    # mean_of_skills = [ 6 ]
    # mean_of_ratings = [ 4.5 ]
    # mean_of_educational_percentage = [ 5.1 ]
    # mean_of_work_percentage = [ 4.5 ]

    # less_of_skills = [ 3 ]
    # less_of_ratings = [ 2.25 ]
    # less_of_educational_percentage = [ 2.55 ]
    # less_of_work_percentage = [ 2.25 ]

    current_iteration = 1

    # print("dataset")
    # print(dataset)

    # Step 1: Initialize Membership Values
    membership_values = initialize_membership_values(c, data_points)

    # Step 2: Initialize Cluster Centers
    mean_list = np.column_stack((mean_of_skills, mean_of_ratings, mean_of_educational_percentage, mean_of_work_percentage))
    less_list = np.column_stack((less_of_skills, less_of_ratings, less_of_educational_percentage, less_of_work_percentage))
    cluster_centers = np.concatenate((mean_list, less_list)) 

    while not stopping_criterion(current_iteration, membership_values, data_points):
        print("Current iteration")
        print(current_iteration)
        if current_iteration > 1:
            # Step 3: Calculate Cluster Centers
            cluster_centers = calculate_cluster_centers(c, membership_values, dataset, data_points, cluster_centers, m)
        cluster_centers = np.round(cluster_centers, 2)
        print("Cluster Centers")
        print(cluster_centers)
        # Step 4: Update Membership Values
        membership_values = update_membership_values(c, membership_values, dataset, data_points, cluster_centers, m)
        membership_values = np.round(membership_values, 2)
        print("Membership Values")
        print(membership_values)
        # Get cluster labels
        data_points_list = get_cluster_labels(data_points, membership_values)
        current_iteration += 1
    return m, cluster_centers, membership_values, data_points_list

# Step 1: Initialize Membership Values
def initialize_membership_values(c, data_points):
    membership_values = np.random.dirichlet(np.ones(c), size=data_points)
    return membership_values

# Step 3: Calculate Cluster Centers or Centroids
def calculate_cluster_centers(c, membership_values, dataset, data_points, cluster_centers, m):
    for j in range(c):
        numerator = 0
        denominator = sum(np.power(membership_values[:,j], m))
        for i in range(data_points):
            numerator += np.multiply(np.power(membership_values[i,j], m), dataset[i])
        initial_cluster_centers = numerator/denominator
        cluster_centers[j] = initial_cluster_centers
    return cluster_centers

# Step 4: Update Membership Values
def update_membership_values(c, membership_values, dataset, data_points, cluster_centers, m):
    p = float(2/(m-1))
    updated_membership_values = 0
    for i in range(data_points):
        denominator = 0
        euclidean_distance = [float(get_euclidean_distance(dataset[i], cluster_centers[j])) for j in range (c)]
        for j in range(c):
            if (0.0 in euclidean_distance):
                if updated_membership_values == 1:
                    updated_membership_values = 0
                else:
                    updated_membership_values = 1
            else:
                denominator = sum([math.pow(float(euclidean_distance[j]/euclidean_distance[k]), p) for k in range(c)])
                updated_membership_values = float(1/denominator)
            membership_values[i,j] = updated_membership_values
    return membership_values   

# Get Euclidean Distance
def get_euclidean_distance(dataset, cluster_centers):
    euclidean_distance_result = np.abs(float(np.sqrt(np.sum((dataset - cluster_centers) ** 2))))
    return euclidean_distance_result

# Stopping Criterion
def stopping_criterion(current_iteration, membership_values, data_points):
    if (data_points > 1): 
        max_iterations = 7
    else:
        max_iterations = 1
    epsilon =  0.01

    calculated_membership_values = membership_values ** current_iteration
    incremented_iteration = current_iteration + 1
    incremented_membership_values = membership_values ** incremented_iteration
    result = np.abs(np.sum(incremented_membership_values - calculated_membership_values))

    if (current_iteration > max_iterations):
        return True
    elif (data_points > 1):  
        if (result < epsilon):
            return True
        if (membership_values[0].any() == 0):
            return True     
    else:
        return False        

# Get Cluster Labels
def get_cluster_labels(data_points, membership_values):
    data_points_list = []
    
    for i in range(data_points):
        first_number = np.where(membership_values[i] == np.amax(membership_values[i]))[0]
        cluster_label = first_number[0]

        if (cluster_label == 0):
            if (membership_values[i][0] >= 0.6):
                data_points_list.append(i)
    return data_points_list

dataset, data_points = get_data()

m, cluster_centers, membership_values, data_points_list = get_fuzzy_c_means_result(dataset, data_points)

# print("Data Points List")
print(data_points_list)