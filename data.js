const jobList = [
    {
      job_title: "IT Manager",
      job_description: "The IT Manager ensures maintenance of a highly available system environment to support Filinvest business units and business applications, by establishing and implementing group-wide server standards in coordination with other co-service providers. He or she is responsible for server sizing, installation, operation, and maintenance. Provides professional, proactive, and timely support of KRAs and issues for resolution.",
      company_name: "Filinvest Alabang, Incorporated",
      company_location: "Muntinlupa City, Mandaluyong City",
      job_field: "Computer/Information Technology",
      skill_requirements: [
        "Database Management",
        "Data Security",
        "Server Configuration",
        "Network Security",
      ],
      educational_qualification: [
        "BS Computer Engineering",
        "BS Electronic & Communications Engineering",
        "BS Information Technology",
        "Licensure Exam Passer",
        "Computer/Information Technology"
      ],
      work_experience: {
        years: [
          5
        ],
        months: [
          0
        ],
        field: [
          "enterprise server administration"
        ],
        level: [
          "Full Time"
        ]
      },
      status: "open"
    },
    {
      job_title: "System Administrator",
      job_description: "At least 3 year(s) of working experience in the related field is required for this position.",
      company_name: "Genie Technologies Inc.",
      company_location: "Makati City",
      job_field: "Computer/Information Technology",
      skill_requirements: [
        "MYSQL",
      ],
      educational_qualification: [
        "BS Information Technology",
        "BS Computer Science",
        "Computer/Information Technology"
      ],
      work_experience: {
        years: [
          3,
        ],
        months: [
          0
        ],
        field: [
          "system administrator",
        ],
        level: [
          "Full Time"
        ]
      },
      status: "open"
    },
    {
      job_title: "Software Developer",
      job_description: "Software Engineer is expected to perform different role in software development life cycle from development, testing, thru maintenance of company’s application.",
      company_name: "AEON Credit Service (Philippines) Inc.",
      company_location: "National Capital Reg",
      job_field: "Computer/Information Technology",
      skill_requirements: [
        "Object Oriented Programming",
        "Software Development Life Cycle",
        "Java",
        "HTML",
        "CSS",
        "API Development", 
        "Angular JS", 
        "Open Source Technologies", 
        "REST APis",
      ],
      educational_qualification: [
        "BS Information Technology",
        "BS Computer Science",
        "Computer/Information Technology"      
      ],
      work_experience: {
        years: [
          2,
          1,
        ],
        months: [
          0,
          0
        ],
        field: [
          "Java, HTML, CSS and JavaScript",
          "API Development, Angular JS, Open Source Technologies and REST APis"
        ],
        level: [
          "Full Time"
        ]
      },
      status: "open"
    },
    {
      job_title: "Android Developer",
      job_description: "Implement excellent User-experience and User-interfacing technologies.",
      company_name: "LYKA",
      company_location: "Taguig City",
      job_field: "Computer/Information Technology",
      skill_requirements: [
        "Android Studio SDK",
        "Object Oriented",
        "Basic API integrator",
        "Good UI/UX analysis"
      ],
      educational_qualification: [
        "Bachelor's/College Degree",
        "Computer/Information Technology"
      ],
      work_experience: {
        years: [
          2
        ],
        months: [
          0
        ],
        field: [
          
        ],
        level: [
          "Full Time"
        ]
      },
      status: "open"
    },
    {
      job_title: "Full Stack Developer",
      job_description: "You will be an important member of their interdisciplinary project teams and develop, test, and ship software that meets their needs while also solving complex software challenges. You will have strong Engineering skills coupled with an analytical and innovative mindset to challenge conventional thinking.",
      company_name: "ETICA Group Pty ltd",
      company_location: "Manila City",
      job_field: "Computer/Information Technology",
      skill_requirements: [
        "HTML",
        "CSS", 
        "JavaScript", 
        "React", 
        "TypeScript", 
        "Redux", 
        "React Router", 
        "Unit Testing Frameworks",
        "Spring Cloud API gateway",
        "SAML",
      ],
      educational_qualification: [
        "Bachelor's/College Degree",
        "Computer/Information Technology"
      ],
      work_experience: {
        years: [
          2
        ],
        months: [
          0
        ],
        field: [
          
        ],
        level: [
          "Full Time"
        ],
      },
      status: "open"
    },
    {
      job_title: "Web Developer",
      job_description: "Web Developer responsible for designing, coding and modifying websites, from layout to function and according to a company's specifications. Strive to create visually appealing sites that feature user-friendly design and clear navigation.",
      company_name: "ROCKBIRD EVENTS MANAGEMENT",
      company_location: "NCR",
      job_field: "Computer/Information Technology",
      skill_requirements: [
        "PHP",
        "MYSQL",
        "Wordpress",
        "HTML5", 
        "CSS3", 
        "JavaScript", 
        "jQuery",
        "Object-Oriented Programming",
        "JSON", 
        "XML",
      ],
      educational_qualification: [
        "Bachelor of Computer Science",
        "Bachelor of Information Technology",
        "Computer/Information Technology"
      ],
      work_experience: {
        years: [
          2
        ],
        months: [
          0
        ],
        field: [
          
        ],
        level: [
          "Full Time"
        ],
      },
      status: "open"
    },
    {
      job_title: "Back End Web Developer",
      job_description: "Back End Web Developer has an ability to communicate clearly and concisely with clients, our sales team, designers and developers.",
      company_name: "Crescendo Digital Marketing Services Limited Co",
      company_location: "NCR",
      job_field: "Computer/Information Technology",
      skill_requirements: [
        "Java",
        "Python", 
        "cms", 
        "springboot", 
        "spring MVC",
      ],
      educational_qualification: [
        "Bachelor's/College Degree",
        "Computer/Information Technology"
      ],
      work_experience: {
        years: [
          1,
          5
        ],
        months: [
          0
        ],
        field: [
          "IT/Computer - Software or equivalent",
          "backend experience including modern Java"
        ],
        level: [
          "Full Time"
        ],
      },
      status: "open"
    },
    {
      job_title: "Software Tester",
      job_description: "The Agile Test Engineer at TwistResources is a full-fledged member of the Scrum Team. He/She holds primary accountability for software quality assurance and testing processes, methods, and techniques for the project, as well as participates in Planning, Estimating, Scheduling, Retrospective, and any other Team Activities.",
      company_name: "TwistResources, Inc.",
      company_location: "Central Luzon, Ilocos Region, National Capital Reg, Calabarzon & Mimaropa",
      job_field: "Computer/Information Technology",
      skill_requirements: [
        "Test Case Management",
        "Performance Testing", 
        "Security Testing", 
        "Regression Testing", 
        "Functional Testing", 
        "Load Testing", 
        "Sanity Testing", 
        "Speed Testing", 
        "API Testing", 
        "Smoke Testing", 
      ],
      educational_qualification: [
        "Bachelor's/College Degree",
        "Professional License (Passed Board/Bar/Professional License Exam)",
        "Computer/Information Technology",
      ],
      work_experience: {
        years: [
          2
        ],
        months: [
          0
        ],
        field: [
          "Test Engineer for Web Applications"
        ],
        level: [
          "Full Time"
        ]
      },
      status: "open"
    },
    {
      job_title: "IT Specialist",
      job_description: "Under the supervision of the MIS Manager, the IT Specialist is responsible for carrying out duties to the following functional areas: System Maintenance, Technical Support for End-users, Members and Guests, Software Development & Other Assigned Duties.",
      company_name: "The Orchard Golf and Country Club",
      company_location: "Cavite",
      job_field: "Computer/Information Technology",
      skill_requirements: [
        "full stack web development",
        "laravel framework", 
        "code igniter", 
        "nodejs/ viber bot", 
        "database administration",
        "network and desktop troubleshooting",
        "data security maintenance"
      ],
      educational_qualification: [
        "Bachelor of Computer Science",
        "Bachelor of Information Technology",
        "Computer/Information Technology"
      ],
      work_experience: {
        years: [
          1
        ],
        months: [
          0
        ],
        field: [
          "IT Support staff and programmer"
        ],
        level: [
          "Full Time"
        ],
      },
      status: "open"
    },
    {
      job_title: "IT Intern",
      job_description: "Will handle IT related functions.",
      company_name: "Amaia Land Corp.",
      company_location: "National Capital Reg",
      job_field: "Computer/Information Technology",
      skill_requirements: [
        "IT related functions"
      ],
      educational_qualification: [
        "Bachelor of Computer Science - Undergraduate",
        "Bachelor of Information Technology - Undergraduate",
        "Computer/Information Technology"
      ],
      work_experience: {
        years: [
          0
        ],
        months: [
          0
        ],
        field: [
          
        ],
        level: [
          "Internship"
        ],
      },
      status: "open"
    },
    {
      job_title: "Senior Software Engineer",
      job_description: "We are currently looking for a Senior Software Engineer specializing in Systems Integration, with at least 5 Years experience in C#, .NET Core, Microservices and Azure.",
      company_name: "Interprit Philippines",
      company_location: "Northern Mindanao, Davao, Caraga, Cebu (Cebu City), Cebu (Others), Zamboanga",
      job_field: "Computer/Information Technology",
      skill_requirements: [
        "REST services",
        ".NET",
        "C#",
        "Entity Framework",
        "Service Bus, Event Hub, Event Grid",
        "API Management",
        "DevOps using Git and ARM Templates"
      ],
      educational_qualification: [
        "Bachelor's Degree in Computer Science",
        "Bachelor's Degree in Information Technology",
        "Bachelor's Degree in Software Engineering",
        "Computer/Information Technology"
      ],
      work_experience: {
        years: [
          7
        ],
        months: [
          0
        ],
        field: [
          "Developer/Software Engineer"
        ],
        level: [
          "Full Time"
        ],
      },
      status: "open"
    },
    {
      job_title: "Network Security Engineer Level 1",
      job_description: "will be responsible for firewall configuration changes, basic troubleshooting, turning up new clients, and following up with clients regarding existing issues.",
      company_name: "Perimeter E-Security Philippines",
      company_location: "Makati City",
      job_field: "Computer/Information Technology",
      skill_requirements: [
        "Working knowledge of the Fortinet Firewall",
        "Prior experience working in a network operations center or security operations center",
        "Hands on experience working with configurations on firewall and other security appliances",
        "Excellent verbal and written communication skills including experience documenting activities in a ticketing system",
        "Passion for learning and exploring the latest and best practices in network security",
        "Thorough understanding of basic networking principles and services",
        "Knowledge of firewalls"
      ],
      educational_qualification: [
        "Bachelor's/College Degree",
        "Computer/Information Technology"
      ],
      work_experience: {
        years: [
          1,
        ],
        months: [
          0
        ],
        field: [
          
        ],
        level: [
          "Full Time"
        ],
      },
      status: "open"
    },
    {
      job_title: "Systems Engineer",
      job_description: "At least 3 years of working experience in the related field is required for this position",
      company_name: "Sumitronics Phils., Inc.",
      company_location: "Laguna (Calamba City)",
      job_field: "Computer/Information Technology",
      skill_requirements: [
        "SQL Programming",
        "Vb.Net",
        "Crystal reports",
        "MS Office Applications",
        "JAVA",
        "C++",
        "C#",
        "Android",
        "HTML",
        "XML"
      ],
      educational_qualification: [
        "Bachelor's/College Degree",
        "Computer/Information Technology"
      ],
      work_experience: {
        years: [
          3
        ],
        months: [
          0
        ],
        field: [
          "Software Development with advance knowledge in SQL Programming and Vb.Net"
        ],
        level: [
          "Full Time"
        ],
      },
      status: "open"
    },
    {
      job_title: "Software Development Analyst",
      job_description: "Create conceptual and logical business solution design for BAU and replicating business requirements, Update relevant documentation pertaining to delivered ERP services and Follow and improve delivery and operation support methodology",
      company_name: "Smart Communications, Inc.",
      company_location: "National Capital Reg",
      job_field: "Computer/Information Technology",
      skill_requirements: [
        "SAP Inventory Management",
        "SAP Hana",
        "Oracle", 
        "SQL", 
        "Visual Basic", 
        "JavaScript", 
        "AUTOCAD", 
        "HTML"
      ],
      educational_qualification: [
        "Bachelor's Degree in Computer Science",
        "Bachelor's Degree in Information Technology",
        "Bachelor's Degree in Computer Engineering",
        "ECE Licensure Examination Passer",
        "ITIL certification",
        "Computer/Information Technology"
      ],
      work_experience: {
        years: [
          2
        ],
        months: [
          0
        ],
        field: [
          "SAP Inventory Management module experience doing configuration and SAP HANA"
        ],
        level: [
          "Full Time"
        ],
      },
      status: "open"
    },
    {
      job_title: "Application IT Engineer",
      job_description: "This position will develops application as a whole or part depending on the design as provided by the SMEs, Senior IT Team and Systems Architect. Application solutions that not only meet functionality requirements but meet performance, scalability and reliability requirements while adhering to implementation schedules, development principles & standard and product goals.",
      company_name: "Rex Education",
      company_location: "National Capital Reg",
      job_field: "Computer/Information Technology",
      skill_requirements: [
        "extensible markup language (xml)", 
        "html programming", 
        "C Language", 
        "Java", 
        "Visual Basic", 
        "CSS"
      ],
      educational_qualification: [
        "Bachelor's Degree in Computer Science",
        "Bachelor's Degree in Information Technology",
        "Computer/Information Technology"
      ],
      work_experience: {
        years: [
          1
        ],
        months: [
          0
        ],
        field: [
          "building application using programming languages like PHP, MySQL, Laravel, Javascript, Jquery, Bootstrap and Web Service/API"
        ],
        level: [
          "Full Time"
        ],
      },
      status: "open"
    },
    {
      job_title: "Copywriter",
      job_description: "The Copywriter is expected to work independently, take feedback constructively and efficiently implement such into future work. They must also be able to communicate confidently with colleagues both from the Philippines and Australia. ",
      company_name: "CAMPAIGNTRACK INC.",
      company_location: "Makati City",
      job_field: "Arts/Media/Communications",
      skill_requirements: [
        "Professional fluency in the English language",
        "Excellent command in writing and proofreading with great attention to detail", 
        "Proven ability to effectively communicate complex information and ideas in an engaging written format", 
        "Strong organisational skills, including the ability to work independently and ability to submit requirements on tight deadlines", 
      ],
      educational_qualification: [
        "Bachelor's Degree in Journalism",
        "Bachelor's Degree in Media",
        "Bachelor's Degree in Communications",
        "Arts/Media/Communications"
      ],
      work_experience: {
        years: [
          1
        ],
        months: [
          0
        ],
        field: [
          "copywriter"
        ],
        level: [
          "Full Time"
        ],
      },
      status: "open"
    },
    {
      job_title: "Digital Marketing Assistant",
      job_description: "This role is responsible for the implementation of customer relationship management (CRM), email marketing and content marketing activities across the company. Reporting to the Team Leader – Digital Marketing and Marketing Manager. (Customer Retention)",
      company_name: "GBSS",
      company_location: "Taguig City",
      job_field: "Arts/Media/Communications",
      skill_requirements: [
        "Adobe Creative Cloud",
        "Strong understanding of the latest direct marketing techniques", 
        "Strong understanding of social media platforms", 
      ],
      educational_qualification: [
        "Bachelor's/College Degree",
        "Arts/Media/Communications"
      ],
      work_experience: {
        years: [
          2
        ],
        months: [
          0
        ],
        field: [
          
        ],
        level: [
          "Full Time"
        ],
      },
      status: "open"
    },
    {
      job_title: "Multimedia Supervisor",
      job_description: "Conceptualize and develop design graphic applications, e.g. collateral material, corporate identity, film titling, multimedia interfaces, etc., based on agreed project briefs and or as may directed by superior.",
      company_name: "Moldex Land Inc.",
      company_location: "Quezon City",
      job_field: "Arts/Media/Communications",
      skill_requirements: [
        "Photoshop",
        "Illustrator", 
        "Aftereffects", 
      ],
      educational_qualification: [
        "Bachelor's Degree in Advertising/Media",
        "Bachelor's Degree in Art/Design/Creative Multimedia",
        "Arts/Media/Communications"
      ],
      work_experience: {
        years: [
          3
        ],
        months: [
          0
        ],
        field: [
          "Arts/Creative/Graphics Design"
        ],
        level: [
          "Full Time"
        ]
      },
      status: "open"
    },
    {
      job_title: "Communications Assistant Manager",
      job_description: "Assist the PR & CorpComm Head to drive and execute communications initiatives through content storytelling, ensuring consistent brand messaging across multiple channels.",
      company_name: "Anchor Land Holdings Inc.",
      company_location: "Makati City",
      job_field: "Arts/Media/Communications",
      skill_requirements: [
        "Monitor and measure online communication activities",
        "Manage partnerships with agencies and influencer", 
      ],
      educational_qualification: [
        "Bachelor's/College Degree",
        "Arts/Media/Communications"
      ],
      work_experience: {
        years: [
          5
        ],
        months: [
          0
        ],
        field: [
          "Assistant Manager/Manager"
        ],
        level: [
          "Full Time"
        ],
      },
      status: "open"
    },
    {
      job_title: "Videographer/Photographer (Multimedia Artist)",
      job_description: "Our company is looking for an experienced, driven and self-motivated Videographer/photographer who will be responsible for capturing, editing and producing high quality photos/videos for both internal and external purposes. You are also to develop a brand strategy for our online platforms.",
      company_name: "Focus Global Inc.",
      company_location: "National Capital Reg",
      job_field: "Arts/Media/Communications",
      skill_requirements: [
        "Adobe Premiere Pro", 
        "After Effects & Creative Suite", 
        "Photoshop", 
        "InDesign", 
        "Lightroom",
        "Illustrator", 
      ],
      educational_qualification: [
        "Bachelor's Degree in Photography",
        "Bachelor's Degree in Film",
        "Bachelor's Degree in Multimedia Arts",
        "Arts/Media/Communications"
      ],
      work_experience: {
        years: [
          1
        ],
        months: [
          0
        ],
        field: [
          "digital media, graphic design, and videography/photography experience"
        ],
        level: [
          "Full Time"
        ],
      },
      status: "open"
    },
    {
      job_title: "Digital Media UI/UX Senior Designer",
      job_description: "Our company requires a UI/UX senior designer, marketing and design all-rounder. The winning candidate should possess a range of skills and be comfortable with a multidisciplinary approach including skillsets in the following areas.",
      company_name: "Brokerpedia",
      company_location: "National Capital Reg",
      job_field: "Arts/Media/Communications",
      skill_requirements: [
        "UX/UI",
        "Illustrator CS6", 
        "photoshop" 
      ],
      educational_qualification: [
        "Bachelor's/College Degree",
        "Passed Board/Bar/Professional License Exam",
        "Arts/Media/Communications"
      ],
      work_experience: {
        years: [
          5
        ],
        months: [
          0
        ],
        field: [
          "UX/UI"
        ],
        level: [
          "Full Time"
        ],
      },
      status: "open"
    },
    {
      job_title: "SOCIAL MEDIA SPECIALIST",
      job_description: "Overall management and handling tasks associated with the company’s social media platforms",
      company_name: "K-Talyst Corporation",
      company_location: "Taguig City",
      job_field: "Arts/Media/Communications",
      skill_requirements: [
        "Excellent interpersonal skills, verbal and written communication skills",
        "High affinity and cultural awareness of political and social situations regarding the relevant market and region that will be supported",
        "fluent in English, must be able to write and speak the language",
        "Has network of influencers is advantage",
        "Knowledge of the latest Facebook / Instagram algorithm is advantage",
        "Possesses creative flair, versatility, conceptual/ visual ability and originality in work"
      ],
      educational_qualification: [
        "Bachelor's/College Degree",
        "Arts/Media/Communications"
      ],
      work_experience: {
        years: [
          1
        ],
        months: [
          0
        ],
        field: [
          "Advertising/Media Planning"
        ],
        level: [
          "Full Time"
        ],
      },
      status: "open"
    },
    {
      job_title: "Technical Marketing Specialist",
      job_description: "Marketing & Technical Specialist should be able to assist the lead product marketer in the development, execution, and monitoring of marketing programs across all teams – Channel, Digital, Event, focusing on Technical PR.",
      company_name: "ASUS PHILIPPINES CORPORATION",
      company_location: "Pasig City",
      job_field: "Arts/Media/Communications",
      skill_requirements: [
        "Must have advance technical knowledge of IT hardware products and mobile devices.",
        "Must be interested in the latest games and gadget trends and has an in-depth knowledge of IT products/industry",
        "product marketing and/or technical PR experience"
      ],
      educational_qualification: [
        "Bachelor's/College Degree",
        "Arts/Media/Communications"
      ],
      work_experience: {
        years: [
          2
        ],
        months: [
          0
        ],
        field: [
          "product marketing and/or technical PR experience."
        ],
        level: [
          "Full Time"
        ],
      },
      status: "open"
    },
    {
      job_title: "Product Designer",
      job_description: "The product designer is in charge of the entire process of creating products and usable experiences, taking into account company goals and starting with defining the problems of real people and thinking about their possible solutions.",
      company_name: "Gank Pte. Ltd.",
      company_location: "Manila City",
      job_field: "Arts/Media/Communications",
      skill_requirements: [
        "mobile and web app design",
        "solid understanding of design principles and frameworks in mobile and web formats"
      ],
      educational_qualification: [
        "Bachelor's/College Degree",
        "Passed Board/Bar/Professional License Exam",
        "Arts/Media/Communications"
      ],
      work_experience: {
        years: [
          3
        ],
        months: [
          0
        ],
        field: [
          "mobile and web app design with solid understanding of design principles and frameworks in mobile and web formats"
        ],
        level: [
          "Full Time"
        ],
      },
      status: "open"
    },
    {
      job_title: "Writer Producer",
      job_description: "The Writer/Producer is a combined role for a creative mind who can write and produce for multi-platform contents. You must be an excellent communicator, able to work independently in finding the most interesting angles of the stories and subjects that you’ll write and produce about.",
      company_name: "Calay Holdings, Inc",
      company_location: "National Capital Reg",
      job_field: "Arts/Media/Communications",
      skill_requirements: [
        "A track record of generating workable ideas for engaging content, resulting in high quality output",
        "Demonstrable ability to write articles, video and social content",
        "Ability to translate difficult technical concepts into easy to understand video and graphic production elements",        
        "Some hands-on video production skills, such as camera operation and/or editing"
      ],
      educational_qualification: [
        "Bachelor's/College Degree",
        "Arts/Media/Communications"
      ],
      work_experience: {
        years: [
          1
        ],
        months: [
          0
        ],
        field: [
          "production role"
        ],
        level: [
          "Full Time"
        ],
      },
      status: "open"
    },
];

const jobFieldsList = [
  {
    job_field: "Accounting/Finance",
    courses: [
      "Accountancy",
      "Banking",
      "Finance",
    ]
  },
  {
    job_field: "Admin/Human Resources",
    courses: [
      "Human Resource Management",
      "Business Studies",
      "Administration",
      "Management",
      "Pyschology",
    ]
  },
  {
    job_field: "Sales/Marketing",
    courses: [
      "Business Studies",
      "Administration",
      "Management",
      "Commerce",
      "Marketing",
    ]
  },
  {
    job_field: "Arts",
    courses: [
      "Fine Arts",
      "Art",
      "Design",
      "Creative Multimedia",
      "Photography",
      "Film",
      "Multimedia Arts",
    ]
  },
  {
    job_field: "Media/Communications",
    courses: [
      "Journalism",
      "Media",
      "Advertising",
      "Communications",
    ]
  },
  {
    job_field: "Services",
    courses: [
      "Business Management",
      "Data Analytics",
      "Commerce",
    ]
  },
  {
    job_field: "Hotel/Restaurant",
    courses: [
      "Hospitality",
      "Tourism",
      "Hotel Management",
    ]
  },
  {
    job_field: "Education/Training",
    courses: [
      "Education"
    ]
  },
  {
    job_field: "Computer/Information Technology",
    courses: [
      "Information Technology",
      "Computer Science",
      "Computer Engineering",
      "Software Engineering"
    ]
  },
  {
    job_field: "Engineering",
    courses: [
      "Electromechanical Engineering",
      "Mechanical Engineering",
      "Electrical Engineering",
      "Chemical Engineering",
    ]
  },
  {
    job_field: "Manufacturing",
    courses: [
      "Nutrition",
    ]
  },
  {
    job_field: "Building/Construction",
    courses: [
      "Civil Engineering",
    ]
  },
  {
    job_field: "Sciences",
    courses: [
      "Food Technology",
      "Microbiology",
      "Chemistry",
    ]
  },
  {
    job_field: "Healthcare",
    courses: [
      "Nursing",
      "Medicine",
      "Surgery",
    ]
  },
];

const graduateLevelsList = [
  {
    elementary_graduate: [
      "elementary graduate"
    ],
    high_school_graduate: [
      "high school graduate", 
      "senior high school graduate",
      "shs graduate"
    ],
    college_undergraduate: [
      "college undergraduate",
      "associate degree",
      "ad"
    ],
    vocational_graduate: [
      "vocational graduate",
    ],
    college_graduate: [
      "bachelor degree graduate", 
      "bachelor degree", 
      "college degree", 
      "bachelor of arts",
      "ba",
      "bachelor of science",
      "bs",
    ],
    board_passer: [
      "board passer",
      "licensure exam passer"
    ],
    masteral_graduate: [
      "masteral degree graduate", 
      "masteral degree",
      "master of arts",
      "master of science"
    ],
    doctorate_graduate: [
      "doctorate degree graduate", 
      "doctorate degree",
      "doctor of arts",
      "doctor of science"
    ]
  }
];

const usersList = [
    {
      name: 'Ian',
      skills: [
        "Network Security",
        "MYSQL",
        "Android Studio",
        "JavaScript",
        "HTML",
        "CSS",
        "PHP",
        "Load Testing",
        "nodejs",
        "Java",
        "Python",
        "cms"
      ],
      ratings: [
        3,
        5,
        7,
        5,
        5,
        5,
        6,
        6,
        7,
        7,
        7,
        7
      ],
      educational_qualification: "BS Computer Science - Licensure Exam Passer",
      job_field: "Computer/Information Technology",
      work_experience: {
        years: [
          3
        ],
        months: [
          0
        ],
        field: [
          "backend experience including modern Java"
        ],
        level: [
          "Full Time"
        ]
      },
    },
    // {
    //   name: 'Jhun',
    //   skills: [
    //     "HTML",
    //     "CSS",
    //     "JavaScript",
    //     "Node.js",
    //     "MS SQL Server", 
    //     "MYSQL", 
    //     "Database Management",
    //     "Data Security",
    //     "Server Configuration",
    //     "Android Studio SDK",
    //     "Object Oriented",
    //     "Basic API integrator",
    //     "Java",
    //     "Python", 
    //     "cms", 
    //   ],
    //   ratings: [
    //     9,
    //     8,
    //     9,
    //     9,
    //     9,
    //     8,
    //     8,
    //     7,
    //     8,
    //     8,
    //     8,
    //     8,
    //     7,
    //     7,
    //     7
    //   ],
    //   educational_qualification: "bs computer science",
    //   job_field: "Computer/Information Technology",
    //   work_experience: {
    //     years: [
    //       2
    //     ],
    //     months: [
    //       0
    //     ],
    //     field: [
    //       "Java, HTML, CSS and JavaScript"
    //     ],
    //     level: [
    //       "Full Time"
    //     ]
    //   },
    // },
    // {
    //   name: 'James',
    //   skills: [
    //     "MYSQL",
    //     "Python",
    //     "API Development",
    //     "Network Security",
    //     "Cisco",
    //   ],
    //   ratings: [
    //     6,
    //     6,
    //     7,
    //     9,
    //     8,
    //     7
    //   ],
    //   educational_qualification: "BS Information Technology",
    //   job_field: "Computer/Information Technology",
    //   work_experience: {
    //     years: [
    //       5
    //     ],
    //     months: [
    //       0
    //     ],
    //     field: [
    //       "enterprise server administration"
    //     ],
    //     level: [
    //       "Full Time"
    //     ]
    //   },
    // },
    // {
    //   name: 'Kezia',
    //   skills: [
    //     "MS SQL Server", 
    //     "MYSQL", 
    //     "Oracle",
    //     "Java",
    //     "English language"
    //   ],
    //   ratings: [
    //     7,
    //     9,
    //     10,
    //     6,
    //     8
    //   ],
    //   educational_qualification: "BS Information Technology",
    //   job_field: "Computer/Information Technology",
    //   work_experience: {
    //     years: [
    //       0
    //     ],
    //     months: [
    //       6
    //     ],
    //     field: [
    //       "system administrator"
    //     ],
    //     level: [
    //       "Full Time"
    //     ]
    //   },
    // },
    // {
    //   name: 'Mark',
    //   skills: [
    //     "MYSQL", 
    //     "Python",
    //     "JavaScript",
    //     "HTML",
    //     "CSS"
    //   ],
    //   ratings: [
    //     6,
    //     7,
    //     9,
    //     8,
    //     8
    //   ],
    //   educational_qualification: "BS Computer Engineering",
    //   job_field: "Computer/Information Technology",
    //   work_experience: {
    //     years: [
    //       2
    //     ],
    //     months: [
    //       0
    //     ],
    //     field: [
    //       ""
    //     ],
    //     level: [
    //       "Full Time"
    //     ]
    //   },
    // },
    // {
    //   name: 'Paul',
    //   skills: [
    //     "CSS",
    //     "JavaScript",
    //     "Node.js",
    //     "MS SQL Server",
    //     "Android Studio",
    //     "Database Management",
    //     "Network Security"
    //   ],
    //   ratings: [
    //     8,
    //     9,
    //     9,
    //     7,
    //     4,
    //     8,
    //     7
    //   ],
    //   educational_qualification: "BS Computer Science",
    //   job_field: "Computer/Information Technology",
    //   work_experience: {
    //     years: [
    //       2
    //     ],
    //     months: [
    //       0
    //     ],
    //     field: [
    //       "Java, HTML, CSS and JavaScript"
    //     ],
    //     level: [
    //       "Full Time"
    //     ]
    //   },
    // },
];

module.exports = {jobList, jobFieldsList, graduateLevelsList, usersList};