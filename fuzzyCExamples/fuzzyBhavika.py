import sys, json
import random
import numpy as np
import pandas as pd
import math

value = json.loads(sys.argv[1])

cluster_number = value["clusterNumber"]

# Get the data that is hardcoded
def get_data():
    jobs = json.dumps(value["clusterLabels"])
    highest_percentage = value["highestPercentage"]
    average_ratings = value["averageRatings"]

    data = {
        'highest_percentage': highest_percentage, 
        'average_ratings': average_ratings, 
        'jobs': jobs 
    }

    df = pd.DataFrame(data=data)
    
    return df      


# Perform Fuzzy C-Means Clustering
def FCM(dataframe, random_state=0, C=4, m=2):
    """
    Fuzzy C-means Clustering. 
    :param dataframe: 
    :param random_state: 
    :param c: int, no. of clusters 
    :param m: int, fuzzifier: the level of cluster fuzziness
    :return: list of predicted labels, list of cluster centroids 
    """

    random.seed(random_state)
    D = dataframe.shape[0]

    data = np.array(dataframe['highest_percentage'],
                    dataframe['average_ratings'])
    labels = np.array(dataframe['jobs'])

    n_iterations = 0

    # Wij is the value in matrix W which tells us how likely it is that value i in data belongs to cluster j
    # i = 4, j = 3 (no. of clusters).
    # Therefore our W matrix is of size (i rows * j columns)

    # Step 1: Initialize the Membership Values: Wij
    W = initialize_membership_values(C, D)
    # Step 2: Calculate the Centroids or Cluster Centers
    centroids = calculate_centroids(data, W, C, D, m)
    # Minimize Jm or the Objective Function
    jm = calculate_objective_function(data, W, C, D, centroids, m)
    jm2 = 0

    # Step 4: Stop if the stopping criterion is met, otherwise return to Step 2
    while not stopping_criterion(n_iterations, jm, jm2):
        jm2 = jm
        # Step 2: Calculate the Centroids or Cluster Centers
        centroids = calculate_centroids(data, W, C, D, m)
        # Step 3: Update Membership Values
        W = update_membership(W, centroids, C, data, m)
        # Get cluster labels
        labels = getClusters(W, D)
        n_iterations += 1 

    return labels, centroids

# Step 1: Initialize the Membership Values: Wij
def initialize_membership_values(C, D):
    """
    Initialize membership values
    :param c: int, no. of clusters 
    :param D: int, no. of datapoints
    :return: W
    """

    W = list()
    for i in range(D):
        c = np.random.dirichlet(np.ones(C) / C, size=1)
        c = c.tolist()
        W.append(c)
    return W
    
# Step 2: Calculate the Centroids or Cluster Centers    
def calculate_centroids(data, W, C, D, m):
    """
    Calculate centroids
    :param data: numpy array
    :param W: list of lists
    :param C: int, no of clusters
    :param D: int, no. of datapoints
    :param m: int, fuzzifier
    :return: centroids 
    """

    centroids = []

    for j in range(C):
        x = []
        for i in range(D):
            x.append(W[i][0][j])
        xraised = [x_i ** m for x_i in x]
        denominator = sum(xraised)
        temp = zip(xraised, data)
        numerator = 0
        for i in temp:
            numerator += i[0] * float(i[1])
        c_j = numerator/denominator
        centroids.append(c_j)
    return centroids

# Minimize Jm or the Objective Function
def calculate_objective_function(data, W, C, D, centroids, m):
    """
    Objective function calculation 
    :param data: numpy array
    :param W: list of lists
    :param C: int, no of clusters
    :param D: int, no. of datapoints
    :param centroids: list, centroids 
    :param m: int, fuzzifier
    :return: int, value of objective function
    """
    mu = 0
    for i in range(D):
        for j in range(C):
            dist = [data[i] - centroids[c] for c in range(C)]
            mu += (float(W[i][0][j]) ** m) * (sum(dist) ** 2)
    return mu

# Step 3: Update Membership Values
def update_membership(W, centroids, C, data, m):
    """
    Update membership values
    :param data: numpy array
    :param W: list of lists
    :param C: int, no of clusters
    :param centroids: list, centroids 
    :param m: int, fuzzifier
    :return: W
    """

    p = float(2/(m-1))
    for i in range(len(data)):
        x = data[i]
        numerator = [x - centroids[j] for j in range(C)]
        for j in range(C):
            denominator = sum([math.pow(float(numerator[j]/numerator[c]), p) for c in range(C)])
            W[i][0][j] = float(1/denominator)
    return W    

# Stopping criterion in Fuzzy C Means Clustering
def stopping_criterion(n_iterations, j_m, j_m2):
    """
    Stopping criterion in fuzzy clustering, either:
        1. n_iterations > max_iterations
        2. |j_m - j_m2| < epsilon
    :param n_iterations: int, current iteration
    :param j_m: int
    :param j_m2: int 
    :return: 
    """
    max_iterations = 4
    epsilon = 0.01
    
    if n_iterations > max_iterations:
        return True
    elif abs(j_m - j_m2) < epsilon:
        return True
    else:
        return False    

# Get cluster labels
def getClusters(W, D):
    """
    For each data point, get original labels
    :param W: list of lists 
    :param D: int, size of dataset
    :return: list, cluster labels
    """
    cluster_labels = list()
    for i in range(D):
        temp = (W[i][0])
        idx = temp.index(max(temp)) # this should be min(temp)
        cluster_labels.append(idx)
    return cluster_labels

dataset = get_data()
results = FCM(dataset)
labels = results[0]

print(labels)








