import random
import numpy as np
import pandas as pd
import math
import operator

cluster_number = 4
m = 1.7 # This should be 2
MAX_ITERATIONS = 4

jobs = [
    'Android Developer', 
    "IT Manager",
    "System Administrator",
    "Software Developer"
]

matching_skills = [ 
    0.8,
    1,
    0.3,
    0.7
]

ratings = [ 
    7.75,
    8.12,
    7.67,
    8.90
]

data = {
    'matching_skills': matching_skills,
    'ratings': ratings,
    'jobs': jobs
}

# Transform the dataset into a DataFrame
main_df = pd.DataFrame(data=data)
columns = list(main_df.columns)
features = columns[:len(columns)-1]
cluster_labels = [columns[-1]]
features_df = main_df[features]
cluster_labels_df = main_df[cluster_labels]
data_points = len(features_df)

# Step 1: Initialize Membership Values
def initializeMembershipValues():
    membership_values_list = []
    for i in range(data_points):
        random_list = [random.random() for c in range(cluster_number)]
        summation = sum(random_list)
        membership_values = [random_number/summation for random_number in random_list]
        membership_values_list.append(membership_values)
    return membership_values_list

# Step 2: Calculate Cluster Centers
def calculateClusterCenters(membership_values):
    cluster_centers = []
    cluster_membership_values = list(membership_values)
    for j in range(cluster_number):
        x = list(cluster_membership_values[j])
        xraised = [x_i ** m for x_i in x]
        denominator = sum(xraised)
        temp_list = []
        for i in range(data_points):
            data_point = list(features_df.iloc[i])
            temp_result = [xraised[i] * x_i for x_i in data_point]
            temp_list.append(temp_result)
        numerator = map(sum, list(zip(*temp_list)))    
        initial_cluster_centers = [num/denominator for num in numerator]
        cluster_centers.append(initial_cluster_centers)
    return cluster_centers

# Step 3: Update the Membership Values
def updateMembershipValues(membership_values, cluster_centers):
    p = float(2/m-1)
    for i in range(data_points):
        x = list(features_df.iloc[i])
        distances = [np.linalg.norm(np.array(list(map(operator.sub, x, cluster_centers[j])))) for j in range(cluster_number)] 
        for j in range(cluster_number):
            denominator = sum([math.pow(float(distances[j]/distances[c]), p) for c in range(cluster_number)])
            membership_values[i][j] = float(1/denominator)
    return membership_values    

# Get Cluster Labels
def getClusterLabels(membership_mat): 
    labels = list()
    for i in range(data_points):
        max_val, idx = max((val, idx) for (idx, val) in enumerate(membership_mat[i]))
        labels.append(idx)
    return labels     

# Implement Fuzzy C-Means Algorithm
def fuzzyCMeans(): 
    current_iteration = 0
    # Step 1: Initialize Membership Matrix 
    membership_values = initializeMembershipValues()
    while current_iteration < MAX_ITERATIONS:
        # Step 2: Calculating Cluster Center
        cluster_centers = calculateClusterCenters(membership_values)
        # Step 3: Update the Membership Values    
        membership_values = updateMembershipValues(membership_values, cluster_centers)
        # Get Cluster Labels
        cluster_labels = getClusterLabels(membership_values)
        current_iteration += 1
    return cluster_labels, cluster_centers    

labels, centroids = fuzzyCMeans()
print(labels)
print(centroids)

