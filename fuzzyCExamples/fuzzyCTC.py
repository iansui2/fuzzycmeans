#Fuzzy c means clustering algorithm
import numpy as np, numpy.random
import pandas as pd
from scipy.spatial import distance

k = 2
p = 4

X = pd.DataFrame([
        [1,2],
        [2,3],
        [9,4],
        [10,1]
        ])
print(X)        

# To import data from disk; "C:\\MachineLearning\\Data" is the path
# where the file "fuzzyData.txt" is there
# X = pd.read_csv('C:\\MachineLearning\\Data\\fuzzyData.txt',header = None)
#print(X)


# Print the number of data and dimension 
n = len(X)
d = len(X.columns)
addZeros = np.zeros((n, 1))
X = np.append(X, addZeros, axis=1)
print("The FCM algorithm: \n")
print("The training data: \n", X)
print("\nTotal number of data: ",n)
print("Total number of features: ",d)
print("Total number of Clusters: ",k)

# # Create an empty array of centers
C = np.zeros((k,d+1))
# print(C)

# # Randomly initialize the weight matrix
weight = np.random.dirichlet(np.ones(k),size=n)
# print("\nThe initial weight: \n", np.round(weight,2))

for it in range(5): # Total number of iterations
    
    # Compute centroid
    for j in range(k):
        denoSum = sum(np.power(weight[:,j],p))
        
        sumMM =0
        for i in range(n):
            mm = np.multiply(np.power(weight[i,j],p),X[i,:])
            sumMM +=mm
        cc = sumMM/denoSum
        C[j] = np.reshape(cc,d+1)
        print("Cluster Centers")
        print(C[j])
 
    #print("\nUpdating the fuzzy pseudo partition")
    for i in range(n):
        denoSumNext = 0
        for j in range(k):
             denoSumNext += np.power(1/distance.euclidean(C[j,0:d], X[i,:d]),1/(p-1))
        for j in range(k):
             w = np.power((1/distance.euclidean(C[j,0:d], X[i,:d])),1/(p-1))/denoSumNext
             weight[i,j] = w  
        print(weight)    
            
# print("\nThe final weights: \n", np.round(weight,2))
    
    
for i in range(n):    
    cNumber = np.where(weight[i] == np.amax(weight[i]))
    X[i,d] = cNumber[0]
print("\nThe data with cluster number: \n", X)