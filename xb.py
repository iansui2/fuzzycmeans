import scipy.spatial
import numpy as np
import fuzzyCMeans

u = fuzzyCMeans.membership_values
x = fuzzyCMeans.dataset
v = fuzzyCMeans.cluster_centers
m = fuzzyCMeans.m

def xb(u, x, v, m):
    n = x.shape[0]

    uij = u ** m

    dxv = get_spatial_distance(x, v, m)
    dvv = get_spatial_distance(v, v, m)

    dvv[dvv == 0.0] = np.inf

    xb = np.sum(uij.T @ dxv)/(n*np.min(dvv))

    return xb

def get_spatial_distance(x, y, m):
    return scipy.spatial.distance.cdist(x, y) ** m

xb_result = xb(u, x, v, m) 
print(xb_result)