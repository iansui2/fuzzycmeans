const express = require("express");
const PythonShell = require("python-shell").PythonShell;

const jobList = require("./data.js").jobList;
const jobFieldsList = require("./data.js").jobFieldsList;
const graduateLevelsList = require("./data.js").graduateLevelsList;
const usersList = require("./data.js").usersList;

const { sum } = require("lodash");

const app = express();
const port = 3001;

var getJobs = function (jobList) {
  var jobLabels = [];
  var jobLabelsSet = [];

  jobList.forEach((job) => {
    var jobTitle = job.job_title;
    jobLabels.push(jobTitle);
    jobLabelsSet = [... new Set(jobLabels)];
  });

  return jobLabelsSet;
};

var getParameters = function(usersList, jobList, jobFieldsList, graduateLevelsList, jobLabels) {
  var jobs = jobLabels;
  var jobDetailsList = [];
  var jobTitles = jobList.map(({ job_title }) => job_title);

  jobs.forEach(job => {
    if (jobs.some((result) => jobTitles.includes(result))) {
      var jobTitleIndex = jobTitles.indexOf(job);
      var jobTitleDetails = jobList[jobTitleIndex];
      jobDetailsList.push(jobTitleDetails);
    }
  });

  for (var i = 0; i < usersList.length; i++) {
    var userString = JSON.stringify(usersList[i]);
    var userDetails = JSON.parse(userString);

    var jobDetailsString;
    var jobDetails;

    var jobFieldsString = JSON.stringify(jobFieldsList);
    var jobFieldsDetails = JSON.parse(jobFieldsString);

    var matchingJobTitles = [];

    var clusterLabels = jobDetailsList.reduce((matchingJobTitlesSet, jobDetail) => {
      jobDetailsString = JSON.stringify(jobDetail);
      jobDetails = JSON.parse(jobDetailsString);

      jobDetails.skill_requirements.forEach((skill_requirements) => {
        userDetails.skills.forEach((skill) => {
          var regexSkill = new RegExp("\\b" + skill + "\\b", "i");
          if (regexSkill.test(skill_requirements)) {
            matchingJobTitles.push(jobDetails.job_title);
          }
        });
      });

      var matchingJobTitlesSet = [... new Set(matchingJobTitles)];

      return matchingJobTitlesSet;
    }, []);

    var matchingJobDetails = [];

    var matchingJobDetailsList = jobDetailsList.reduce((matchingJobDetailsSet, jobDetail) => {
      jobDetailsString = JSON.stringify(jobDetail);
      jobDetails = JSON.parse(jobDetailsString);

      jobDetails.skill_requirements.forEach((skill_requirements) => {
        userDetails.skills.forEach((skill) => {
          var regexSkill = new RegExp("\\b" + skill + "\\b", "i");
          if (regexSkill.test(skill_requirements)) {
            matchingJobDetails.push(jobDetails);
          }
        });
      });  

      var matchingJobDetailsSet = [... new Set(matchingJobDetails)];

      return matchingJobDetailsSet;
    }, []);

    var usersSkills = [];

    var usersSkillsList = matchingJobDetailsList.reduce((usersSkillsList, jobDetail) => {
      usersSkills = [];     

      jobDetailsString = JSON.stringify(jobDetail);
      jobDetails = JSON.parse(jobDetailsString);

      jobDetails.skill_requirements.forEach((skill_requirements) => {
        userDetails.skills.forEach((skill) => {
          var regexSkill = new RegExp("\\b" + skill + "\\b", "i");
          if (regexSkill.test(skill_requirements)) { 
            if (skill != "") {
              usersSkills.push(skill);   
            }
          }
        });
      });  

      usersSkillsList.push(usersSkills);
    
      return usersSkillsList;
    }, []);

    var percentageOfMatchingSkillsList = matchingJobDetailsList.reduce((percentageOfMatchingSkillsList, jobDetail) => {
      var numberOfMatchingSkills = 0;
      var numberOfMatchingSkillsList = [];

      var percentageOfMatchingSkills = 0;

      jobDetailsString = JSON.stringify(jobDetail);
      jobDetails = JSON.parse(jobDetailsString);

      jobDetails.skill_requirements.forEach((skill_requirements) => {
        userDetails.skills.forEach(skill => {
          var regexSkill = new RegExp("\\b" + skill + "\\b", "i");
          if (regexSkill.test(skill_requirements)) {
            if (skill != "") {
              numberOfMatchingSkills++;
            }
          }
        }); 
      });  

      numberOfMatchingSkillsList.push(numberOfMatchingSkills);
      numberOfMatchingSkills = 0;

      numberOfMatchingSkillsList.forEach(numberOfMatchingSkills => {
        var length = jobDetails.skill_requirements.length;
        if (jobDetails.skill_requirements.length > 9) {
          length = length / 2;
        } 

        if (numberOfMatchingSkills > length) { 
          percentageOfMatchingSkills = 1;
        } else {
          percentageOfMatchingSkills = numberOfMatchingSkills / length;
        }
      });
  
      var fixedPrecisionMatchingSkillsPercentage = Math.round(percentageOfMatchingSkills * 100) / 100;
      fixedPrecisionMatchingSkillsPercentage *= 10;
      fixedPrecisionMatchingSkillsPercentage = Math.round(fixedPrecisionMatchingSkillsPercentage * 100) / 100;
      percentageOfMatchingSkillsList.push(fixedPrecisionMatchingSkillsPercentage);
    
      return percentageOfMatchingSkillsList;
    }, []);

    var usersRatings = [];

    var averageRatingsList = usersSkillsList.reduce((averageRatingsList, usersSkills) => {
      var usersRatingsList = [];
      usersRatings = userDetails.ratings;

      var averageRatings;

      var fixedPrecisionRatingsPercentage = 0;

      usersSkills.forEach(skill => {
        var usersSkillsIndex = userDetails.skills.indexOf(skill);
        var usersSkillsRatings = usersRatings[usersSkillsIndex];
        usersRatingsList.push(usersSkillsRatings);
      });

      if (usersSkills.length == 0) {
        fixedPrecisionRatingsPercentage = 0;
      } else {
        averageRatings = sum(usersRatingsList) / usersRatingsList.length;
        fixedPrecisionRatingsPercentage = Math.round(averageRatings * 100) / 100; 
      }

      averageRatingsList.push(fixedPrecisionRatingsPercentage);

      return averageRatingsList;
    }, []);

    var graduateLevelsString = JSON.stringify(graduateLevelsList[0]);
    var graduateLevelsDetails = JSON.parse(graduateLevelsString);

    var elementaryGraduateList = graduateLevelsDetails.elementary_graduate;
    var highSchoolGraduateList = graduateLevelsDetails.high_school_graduate;
    var collegeUnderGraduateList = graduateLevelsDetails.college_undergraduate;
    var vocationalGraduateList = graduateLevelsDetails.vocational_graduate;
    var collegeGraduateList = graduateLevelsDetails.college_graduate;  
    var boardPasserList = graduateLevelsDetails.board_passer;
    var masteralGraduateList = graduateLevelsDetails.masteral_graduate;
    var doctorateGraduateList = graduateLevelsDetails.doctorate_graduate;

    var elementaryGraduateLevel = "Elementary Graduate";
    var highSchoolGraduateLevel = "High School Graduate";
    var collegeUnderGraduateLevel = "College Undergraduate";
    var vocationalGraduateLevel = "Vocational Graduate";
    var collegeGraduateLevel = "College Graduate";
    var boardPasserLevel = "College Graduate - Board Passer";  
    var masteralGraduateLevel = "Masteral Graduate";
    var doctorateGraduateLevel = "Doctorate Graduate";
    var boardPasserQualification = "Board Passer/Licensure Exam Passer";

    var userGraduateLevel = "";
    var educationalPercentage = 0;

    elementaryGraduateList.forEach((elementaryGraduate) => {
      if (userDetails.educational_qualification.toLowerCase().includes(elementaryGraduate.toLowerCase())) {
        userGraduateLevel = elementaryGraduateLevel;
      } 
    });

    highSchoolGraduateList.forEach((highSchoolGraduate) => {
      if (userDetails.educational_qualification.toLowerCase().includes(highSchoolGraduate.toLowerCase())) {
        userGraduateLevel = highSchoolGraduateLevel;
      }
    });

    collegeUnderGraduateList.forEach((collegeUnderGraduate) => {
      if (userDetails.educational_qualification.toLowerCase().includes(collegeUnderGraduate.toLowerCase())) {
        userGraduateLevel = collegeUnderGraduateLevel;
      }
    });

    vocationalGraduateList.forEach((vocationalGraduate) => {
      if (userDetails.educational_qualification.toLowerCase().includes(vocationalGraduate.toLowerCase())) {
        userGraduateLevel = vocationalGraduateLevel;
      }
    });

    collegeGraduateList.forEach((collegeGraduate) => {
      if (userDetails.educational_qualification.toLowerCase().includes(collegeGraduate.toLowerCase())) {
        userGraduateLevel = collegeGraduateLevel;
      }
    });

    boardPasserList.forEach((boardPasser) => {
      if (userDetails.educational_qualification.toLowerCase().includes(boardPasser.toLowerCase())) {
        userGraduateLevel = boardPasserLevel;
      }
    });

    masteralGraduateList.forEach((masteralGraduate) => {
      if (userDetails.educational_qualification.toLowerCase().toLowerCase().includes(masteralGraduate.toLowerCase())) {
        userGraduateLevel = masteralGraduateLevel;
      }
    });

    doctorateGraduateList.forEach((doctorateGraduate) => {
      if (userDetails.educational_qualification.toLowerCase().includes(doctorateGraduate.toLowerCase())) {
        userGraduateLevel = doctorateGraduateLevel;
      }  
    });

    var educationalPercentageList = matchingJobDetailsList.reduce((educationalPercentageList, jobDetail) => {
      var educationalQualification = [];
        
      if (jobDetail.educational_qualification != undefined) {
        educationalQualification = jobDetail.educational_qualification.map(education => education.toLowerCase());
      }

      matchedEducationalQualification = false;
      matchedJobField = false;
      boardPasserQualification = false;
      matchedCourse = false;

      educationalQualification.forEach((educationalLevel) => {
        if (userDetails.educational_qualification.toLowerCase().includes(educationalLevel)) {
          matchedEducationalQualification = true;
        }

        if (educationalLevel.includes(userDetails.job_field.toLowerCase())) {
          matchedJobField = true;
        }

        boardPasserList.forEach((boardPasser) => {
          if (educationalLevel.includes(boardPasser)) {
            boardPasserQualification = true;
          }
        });

      });

      var jobFieldIndex = jobFieldsDetails.map(job => job.job_field).indexOf(userDetails.job_field);
      var jobFieldCourses = jobFieldsDetails[jobFieldIndex].courses;

      jobFieldCourses.forEach((course) => {
        if (userDetails.educational_qualification.toLowerCase().includes(course.toLowerCase())) {
          matchedCourse = true;
        }  
      });

      var index = matchingJobDetailsList.indexOf(jobDetail);

      if (matchedEducationalQualification) {
        if (usersSkillsList[index][0] != "" && usersSkillsList[index][0] != undefined) {
          if (boardPasserQualification) {
            if (userGraduateLevel == boardPasserLevel) {
              if (percentageOfMatchingSkillsList[index] >= 5) {
                educationalPercentage = 10;
              } else {
                educationalPercentage = 7;
              }
            } else {
              if (percentageOfMatchingSkillsList[index] >= 5) {
                educationalPercentage = 7;
              } else {
                educationalPercentage = 5;
              }
            }
          } else {
            if (percentageOfMatchingSkillsList[index] >= 5) {
              educationalPercentage = 10;
            } else {
              educationalPercentage = 7;
            }
          }
        } else if (userGraduateLevel === elementaryGraduateLevel) {
          educationalPercentage = 1.5;
        } else if (userGraduateLevel === highSchoolGraduateLevel) {
          educationalPercentage = 3;
        } else if (userGraduateLevel === collegeUnderGraduateLevel) {
          educationalPercentage = 3.8;
        } else if (userGraduateLevel === vocationalGraduateLevel) {
          educationalPercentage = 4.5;
        } else if (userGraduateLevel === collegeGraduateLevel) {
          educationalPercentage = 5;
        } else if (userGraduateLevel === boardPasserLevel) {
          educationalPercentage = 5.8;
        } else if (userGraduateLevel === masteralGraduateLevel) {
          educationalPercentage = 6.5;
        } else if (userGraduateLevel === doctorateGraduateLevel) {
          educationalPercentage = 7;
        } else {
          educationalPercentage = 0;
        }
      } else if (matchedJobField) {
        if (userDetails.job_field === jobDetail.job_field) {
          if (usersSkillsList[index][0] != "" && usersSkillsList[index][0] != undefined) {
            if (matchedCourse) {
              if (boardPasserQualification) {
                if (userGraduateLevel == boardPasserLevel) {
                  if (percentageOfMatchingSkillsList[index] >= 5) {
                    educationalPercentage = 10;
                  } else {
                    educationalPercentage = 7;
                  }
                } else {
                  if (percentageOfMatchingSkillsList[index] >= 5) {
                    educationalPercentage = 7;
                  } else {
                    educationalPercentage = 5;
                  }
                }
              } else {
                if (percentageOfMatchingSkillsList[index] >= 5) {
                  educationalPercentage = 10;
                } else {
                  educationalPercentage = 7;
                }
              }
            } else {
              educationalPercentage = 5;
            }
          } else if (userGraduateLevel === elementaryGraduateLevel) {
            educationalPercentage = 1.5;
          } else if (userGraduateLevel === highSchoolGraduateLevel) {
            educationalPercentage = 3;
          } else if (userGraduateLevel === collegeUnderGraduateLevel) {
            educationalPercentage = 3.8;
          } else if (userGraduateLevel === vocationalGraduateLevel) {
            educationalPercentage = 4.5;
          } else if (userGraduateLevel === collegeGraduateLevel) {
            educationalPercentage = 5;
          } else if (userGraduateLevel === boardPasserLevel) {
            educationalPercentage = 5.8;
          } else if (userGraduateLevel === masteralGraduateLevel) {
            educationalPercentage = 6.5;
          } else if (userGraduateLevel === doctorateGraduateLevel) {
            educationalPercentage = 7;
          } else {
            educationalPercentage = 0;
          }
        }
      } else {
          if (userGraduateLevel === elementaryGraduateLevel) {
            educationalPercentage = 1.5;
          } else if (userGraduateLevel === highSchoolGraduateLevel) {
            educationalPercentage = 3;
          } else if (userGraduateLevel === collegeUnderGraduateLevel) {
            educationalPercentage = 3.8;
          } else if (userGraduateLevel === vocationalGraduateLevel) {
            educationalPercentage = 4.5;
          } else if (userGraduateLevel === collegeGraduateLevel) {
            educationalPercentage = 5;
          } else if (userGraduateLevel === boardPasserLevel) {
            educationalPercentage = 5.8;
          } else if (userGraduateLevel === masteralGraduateLevel) {
            educationalPercentage = 6.5;
          } else if (userGraduateLevel === doctorateGraduateLevel) {
            educationalPercentage = 7;
          } else {
            educationalPercentage = 0;
          }
      }

      educationalPercentageList.push(educationalPercentage);
      return educationalPercentageList;
    }, []);

    var workExperiencePercentage = 0.0;

    var workExperiencePercentageList = matchingJobDetailsList.reduce((workExperiencePercentageList, jobDetail) => {
      var workExperienceJobField = [];
      var workExperienceLevel = [];

      if (jobDetail.work_experience.field != undefined) {
        workExperienceJobField = jobDetail.work_experience.field.map(field => field.toLowerCase());
      } 

      if (jobDetail.work_experience.level != undefined) {
        workExperienceLevel = jobDetail.work_experience.level.map(level => level.toLowerCase());
      }

      var index = matchingJobDetailsList.indexOf(jobDetail);

      if (jobDetail.work_experience.years.length > 1) {
        jobDetail.work_experience.years = [];
        jobDetail.work_experience.years.push(sum(jobDetail.work_experience.years));
      }

      if (userDetails.work_experience.years.length > 1) {
        userDetails.work_experience.years = [];
        userDetails.work_experience.years.push(sum(userDetails.work_experience.years));
      }

      if (userDetails.work_experience.months.length > 1) {
        userDetails.work_experience.months = [];
        userDetails.work_experience.months.push(sum(userDetails.work_experience.months));
      }
      
      userDetails.work_experience.field.forEach((userJobField) => {
        userDetails.work_experience.level.forEach((userLevel) => {
          if (userDetails.work_experience.years[0] >= jobDetail.work_experience.years[0]) {
            if (workExperienceJobField != []) {
              if (workExperienceJobField.includes(userJobField.toLowerCase())) {
                if (workExperienceLevel != []) {
                  if (workExperienceLevel.includes(userLevel.toLowerCase())) {
                    if (percentageOfMatchingSkillsList[index] >= 5) {
                      workExperiencePercentage = 10;
                    } else {
                      workExperiencePercentage = 7;
                    }
                  } else {
                    if (percentageOfMatchingSkillsList[index] >= 5) {
                        workExperiencePercentage = 7;
                    } else {
                      workExperiencePercentage = 5;
                    }
                  }
                } else {
                  if (percentageOfMatchingSkillsList[index] >= 5) {
                    workExperiencePercentage = 7;
                  } else {
                    workExperiencePercentage = 5;
                  }
                }
              } else {
                if (percentageOfMatchingSkillsList[index] >= 5) {
                  workExperiencePercentage = 5;
                } else {
                  workExperiencePercentage = 3;
                }
              }
            } else {
              if (percentageOfMatchingSkillsList[index] >= 5) {
                workExperiencePercentage = 5;
              } else {
                workExperiencePercentage = 3;
              }
            }
          } else {
            if (userDetails.work_experience.years[0] == 0 && (userDetails.work_experience.months[0] == 0 || userDetails.work_experience.months[0] <= 12)) {
              if (workExperienceJobField != []) {
                if (workExperienceJobField.includes(userJobField.toLowerCase())) {
                  if (workExperienceLevel != []) {
                    if (workExperienceLevel.includes(userLevel.toLowerCase())) {
                      if (percentageOfMatchingSkillsList[index] >= 5) {
                        workExperiencePercentage = 2;
                      } else {
                        workExperiencePercentage = 1.5;
                      }
                    } else {
                      if (percentageOfMatchingSkillsList[index] >= 5) {
                          workExperiencePercentage = 1.5;
                      } else {
                          workExperiencePercentage = 1.2;
                      }    
                    }
                  } else {
                    if (percentageOfMatchingSkillsList[index] >= 5) {
                      workExperiencePercentage = 1.5;
                    } else {
                      workExperiencePercentage = 1.2;
                    } 
                  }
                } else {
                  if (percentageOfMatchingSkillsList[index] >= 5) {
                    workExperiencePercentage = 1.2;
                  } else {
                    workExperiencePercentage = 1;
                  } 
                }
              } else {
                if (percentageOfMatchingSkillsList[index] >= 5) {
                  workExperiencePercentage = 1.2;
                } else {
                  workExperiencePercentage = 1;
                }
              }
            } else if (userDetails.work_experience.years[0] == 1) {
              if (workExperienceJobField != []) {
                if (workExperienceJobField.includes(userJobField.toLowerCase())) {
                  if (workExperienceLevel != []) {
                    if (workExperienceLevel.includes(userLevel.toLowerCase())) {
                      if (percentageOfMatchingSkillsList[index] >= 5) {
                        workExperiencePercentage = 3;
                      } else {
                        workExperiencePercentage = 2;
                      }
                    } else {
                      if (percentageOfMatchingSkillsList[index] >= 5) {
                        workExperiencePercentage = 2;
                      } else {
                        workExperiencePercentage = 1.5;
                      }
                    }
                  } else {
                    if (percentageOfMatchingSkillsList[index] >= 5) {
                      workExperiencePercentage = 2;
                    } else {
                      workExperiencePercentage = 1.5;
                    }
                  }
                } else {
                  if (percentageOfMatchingSkillsList[index] >= 5) {
                    workExperiencePercentage = 1.5;
                  } else {
                    workExperiencePercentage = 1.2;
                  }
                }
              } else {
                if (percentageOfMatchingSkillsList[index] >= 5) {
                  workExperiencePercentage = 1.5;
                } else {
                  workExperiencePercentage = 1.2;
                }
              }
            } else if (userDetails.work_experience.years[0] > 1) {
              if (workExperienceJobField != []) {
                if (workExperienceJobField.includes(userJobField.toLowerCase())) {
                  if (workExperienceLevel != []) {
                    if (workExperienceLevel.includes(userLevel.toLowerCase())) {
                      if (percentageOfMatchingSkillsList[index] >= 5) {
                        workExperiencePercentage = 5;
                      } else {
                        workExperiencePercentage = 3;
                      }
                    } else {
                      if (percentageOfMatchingSkillsList[index] >= 5) {
                        workExperiencePercentage = 4;
                      } else {
                        workExperiencePercentage = 2.5;
                      }
                    }
                  } else {
                    if (percentageOfMatchingSkillsList[index] >= 5) {
                      workExperiencePercentage = 4;
                    } else {
                      workExperiencePercentage = 2.5;
                    }
                  }
                } else {
                  if (percentageOfMatchingSkillsList[index] >= 5) {
                    workExperiencePercentage = 3;
                  } else {
                    workExperiencePercentage = 2;
                  }
                }
              } else {
                if (percentageOfMatchingSkillsList[index] >= 5) {
                  workExperiencePercentage = 3;
                } else {
                  workExperiencePercentage = 2;
                }
              }
            } else {
              workExperiencePercentage = 0;
            }  
          }
        });
      });

      workExperiencePercentageList.push(workExperiencePercentage);
      return workExperiencePercentageList;
    }, []);

    var meanOfSkillsRequirementList = [];
    var lengthOfSkillsRequirement = 0;
    var meanOfSkillsRequirement = 0;

    matchingJobDetailsList.forEach((jobDetails) => {
      var length = jobDetails.skill_requirements.length;
      if (length > 9) {
        length = length / 2;
      }
      lengthOfSkillsRequirement += length;
    });

    meanOfSkillsRequirement = lengthOfSkillsRequirement / matchingJobDetailsList.length;
    meanOfSkillsRequirement = Math.round(meanOfSkillsRequirement * 100) / 100;
    meanOfSkillsRequirementList.push(meanOfSkillsRequirement);

    var meanOfRatingsList = [];
    var meanOfRatings = 0;
    var sumOfRatingsRequirement = 0;
    matchingJobDetailsList.forEach((jobDetail) => {
      jobDetail.work_experience.years.forEach((year) => {
        if (year == 0 && (jobDetail.work_experience.months == 0 || jobDetail.work_experience.months <= 12)) {
          meanOfRatings = 2;
        } else if (year == 1) {
          meanOfRatings = 3;
        } else if (year > 1) {
          meanOfRatings = 5;
        } 
      });
      sumOfRatingsRequirement += meanOfRatings; 
    });
    meanOfRatings = sumOfRatingsRequirement /matchingJobDetailsList.length;
    meanOfRatings = Math.round(meanOfRatings * 100) / 100;
    meanOfRatingsList.push(meanOfRatings);

    var meanOfEducationalPercentageList = [];
    var numberOfEducationalPercentageRequirement = 0;
    var meanOfEducationalPercentage = 0;
    var educationalPercentageRequirement = 0;

    matchingJobDetailsList.forEach((jobDetails) => {
      jobDetails.educational_qualification.forEach((educationalQualification) => {
        elementaryGraduateList.forEach((elementaryGraduate) => {
          if (educationalQualification.toLowerCase().includes(elementaryGraduate.toLowerCase())) {
            educationalPercentageRequirement = 1.5;
          } 
        });
    
        highSchoolGraduateList.forEach((highSchoolGraduate) => {
          if (educationalQualification.toLowerCase().includes(highSchoolGraduate.toLowerCase())) {
            educationalPercentageRequirement = 3;
          }
        });
    
        collegeUnderGraduateList.forEach((collegeUnderGraduate) => {
          if (educationalQualification.toLowerCase().includes(collegeUnderGraduate.toLowerCase())) {
            educationalPercentageRequirement = 3.8;
          }
        });
    
        vocationalGraduateList.forEach((vocationalGraduate) => {
          if (educationalQualification.toLowerCase().includes(vocationalGraduate.toLowerCase())) {
            educationalPercentageRequirement = 4.5;
          }
        });
    
        collegeGraduateList.forEach((collegeGraduate) => {
          if (educationalQualification.toLowerCase().includes(collegeGraduate.toLowerCase())) {
            educationalPercentageRequirement = 5;
          }
        });

        jobFieldsDetails.forEach((jobFieldsDetail) => {
          if (educationalQualification.includes(jobFieldsDetail.courses)) {
            educationalPercentageRequirement = 5;
          }
        });

        boardPasserList.forEach((boardPasser) => {
          if (educationalQualification.toLowerCase().includes(boardPasser.toLowerCase())) {
            educationalPercentageRequirement = 5.8;
          }
        });
    
        masteralGraduateList.forEach((masteralGraduate) => {
          if (educationalQualification.toLowerCase().toLowerCase().includes(masteralGraduate.toLowerCase())) {
            educationalPercentageRequirement = 6.5;
          }
        });
    
        doctorateGraduateList.forEach((doctorateGraduate) => {
          if (userDetails.educational_qualification.toLowerCase().includes(doctorateGraduate.toLowerCase())) {
            educationalPercentageRequirement = 7;
          }  
        });
      });
      numberOfEducationalPercentageRequirement += educationalPercentageRequirement;
    });

    meanOfEducationalPercentage = numberOfEducationalPercentageRequirement / matchingJobDetailsList.length;
    meanOfEducationalPercentage = Math.round(meanOfEducationalPercentage * 100) / 100;
    meanOfEducationalPercentageList.push(meanOfEducationalPercentage);

    var meanOfWorkPercentageList = [];
    var numberOfWorkPercentageRequirement = 0;
    var meanOfWorkPercentage = 0;
    var workPercentageRequirement = 0;

    matchingJobDetailsList.forEach((jobDetail) => {
      jobDetail.work_experience.years.forEach((year) => {
        if (year == 0 && (jobDetail.work_experience.months == 0 || jobDetail.work_experience.months <= 12)) {
          workPercentageRequirement = 2;
        } else if (year == 1) {
          workPercentageRequirement = 3;
        } else if (year > 1) {
          workPercentageRequirement = 5;
        } 
      });
      numberOfWorkPercentageRequirement += workPercentageRequirement; 
    });

    meanOfWorkPercentage = numberOfWorkPercentageRequirement / matchingJobDetailsList.length;
    meanOfWorkPercentage = Math.round(meanOfWorkPercentage * 100) / 100;
    meanOfWorkPercentageList.push(meanOfWorkPercentage);

    var lessOfSkillsRequirement = 0;
    var lessOfSkillsRequirementList = []; 

    lessOfSkillsRequirement = meanOfSkillsRequirement / 2;
    lessOfSkillsRequirement =  Math.round(lessOfSkillsRequirement * 100) / 100;
    lessOfSkillsRequirementList.push(lessOfSkillsRequirement);

    var lessOfRatings = 0;
    var lessOfRatingsList = [];

    lessOfRatings = meanOfRatings / 2;
    lessOfRatings = Math.round(lessOfRatings * 100) / 100;
    lessOfRatingsList.push(lessOfRatings);

    var lessOfEducationalPercentage = 0;
    var lessOfEducationalPercentageList = [];

    lessOfEducationalPercentage = meanOfEducationalPercentage / 2;
    lessOfEducationalPercentage = Math.round(lessOfEducationalPercentage * 100) / 100;
    lessOfEducationalPercentageList.push(lessOfEducationalPercentage);

    var lessOfWorkPercentage = 0;
    var lessOfWorkPercentageList = [];

    lessOfWorkPercentage = meanOfWorkPercentage / 2;
    lessOfWorkPercentage = Math.round(lessOfWorkPercentage * 100) / 100;
    lessOfWorkPercentageList.push(lessOfWorkPercentage);
  }

  return [clusterLabels, percentageOfMatchingSkillsList, averageRatingsList, educationalPercentageList, workExperiencePercentageList, meanOfSkillsRequirementList, meanOfRatingsList, meanOfEducationalPercentageList, meanOfWorkPercentageList, lessOfSkillsRequirementList, lessOfRatingsList, lessOfEducationalPercentageList, lessOfWorkPercentageList];
};

if (jobList != null && usersList != null) {
  var jobRecommendations = [];

  var jobLabels = getJobs(jobList);

  var clusterParameters = getParameters(usersList, jobList, jobFieldsList, graduateLevelsList, jobLabels);
  var clusterLabels = clusterParameters[0];
  var highestPercentage = clusterParameters[1];
  var averageRatings = clusterParameters[2];
  var educationalPercentage = clusterParameters[3];  
  var workExperiencePercentage = clusterParameters[4];
  var meanOfSkillsRequirement = clusterParameters[5];
  var meanOfRatings = clusterParameters[6];
  var meanOfEducationalPercentage = clusterParameters[7];
  var meanOfWorkPercentage = clusterParameters[8];
  var lessOfSkillsRequirement = clusterParameters[9];
  var lessOfRatings = clusterParameters[10];
  var lessOfEducationalPercentage = clusterParameters[11];
  var lessOfWorkPercentage = clusterParameters[12];

  if (highestPercentage != undefined && averageRatings != undefined) {
    var skillsRequirementZeroes = highestPercentage.every(item => item === 0);
    var averageRatingsZeroes = averageRatings.every(item => item === 0);
  }

  if (highestPercentage != undefined && averageRatings != undefined && educationalPercentage != undefined && workExperiencePercentage != undefined && !skillsRequirementZeroes && !averageRatingsZeroes) {    
    const parameters = {
      "clusterLabels": clusterLabels,
      "highestPercentage": highestPercentage,
      "averageRatings": averageRatings,
      "educationalPercentage": educationalPercentage,
      "workExperiencePercentage": workExperiencePercentage,
      "meanOfSkills": meanOfSkillsRequirement,
      "meanOfRatings": meanOfRatings,
      "meanOfEducationalPercentage": meanOfEducationalPercentage,
      "meanOfWorkPercentage": meanOfWorkPercentage,
      "lessOfSkills": lessOfSkillsRequirement,
      "lessOfRatings": lessOfRatings,
      "lessOfEducationalPercentage": lessOfEducationalPercentage,
      "lessOfWorkPercentage": lessOfWorkPercentage
    };
      
    let options = {
      mode: 'json',
      pythonOptions: ['-u'], 
      args: [JSON.stringify(parameters)]
    };
      
    PythonShell.run("fuzzyCMeans.py", options, function (err, results) {
        if (err) throw err;
  
        console.log(results);
  
        var jobResults = results.flat();
  
        if (jobResults != null) {
          PythonShell.run("kwon.py", options, function (err, results) {
            if (err) throw err;
              console.log(results);
            }
          );
        }
  
        if (jobResults != null) {
          jobResults.forEach((jobResult) => {
            jobRecommendations.push(clusterLabels[jobResult]);
          });
        } else {
          jobRecommendations.push("No Job Recommendations!");
        }
  
        console.log(jobRecommendations);
  
      });
  } else {
     jobRecommendations = [];
     console.log(jobRecommendations);
  }
}  
  
app.get("/*", (req, res) => {  
  res.write("Result: " + jobRecommendations);
  res.end();
});
  
app.listen(port, () => {
  console.log("Server running on port " + port);
  console.log("Fuzzy C-Means Algorithm implemented!");
  console.log("Check out localhost:3001 for the results.");
  console.log("Cluster Labels");
  console.log(clusterLabels);
  console.log("Highest Percentage");
  console.log(highestPercentage);
  console.log("Average Ratings");
  console.log(averageRatings);
  console.log("Educational Percentage");
  console.log(educationalPercentage);
  console.log("Work Experience Percentage");
  console.log(workExperiencePercentage);
  console.log("meanOfSkillsRequirement");
  console.log(meanOfSkillsRequirement);
  console.log("meanOfRatings");
  console.log(meanOfRatings);
  console.log("meanOfEducationalPercentage");
  console.log(meanOfEducationalPercentage);
  console.log("meanOfWorkPercentage");
  console.log(meanOfWorkPercentage);
  console.log("lessOfSkillsRequirement");
  console.log(lessOfSkillsRequirement);
  console.log("lessOfRatings");
  console.log(lessOfRatings);
  console.log("lessOfEducationalPercentage");
  console.log(lessOfEducationalPercentage);
  console.log("lessOfWorkPercentage");
  console.log(lessOfWorkPercentage);
});

